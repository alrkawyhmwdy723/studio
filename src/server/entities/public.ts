export interface IPublic {
    /** Public key of an item. */
    publicKey: string;
}
