import { GraphQLError, GraphQLScalarType, Kind, ValueNode } from "graphql";
import { IShareSettings } from ".";

function parseValue(value: IShareSettings): IShareSettings {
    return value;
}

function serialize(value?: IShareSettings): IShareSettings | undefined {
    return value;
}

function parseLiteral(ast: ValueNode): IShareSettings {
    // Here we can validate the input
    if (ast.kind === Kind.STRING) {
        return parseValue(JSON.parse(ast.value));
    }
    throw new GraphQLError(`Can only validate definition as string, but got a: ${ast.kind}`);
}

export const shareSettings = new GraphQLScalarType({
    name: "shareSettings",
    description: "Share settings of a user.",
    parseValue,
    serialize,
    parseLiteral,
});
