import { IDefinition as ITripettoDefinition } from "tripetto";
import { IPublic } from "../public";
import { TRunners } from "./runners";
import { TL10n, TStyles } from "tripetto-runner-foundation";

export interface IServerDefinitionInput {
    /** Id of the definition. */
    id: string;

    /** Name of the definition. */
    name: string;

    /** Data of the definition. */
    data?: ITripettoDefinition;
}

export interface IServerDefinition extends IServerDefinitionInput, IPublic {
    /** Token to read the definition with. */
    readToken?: string;

    /** Contains the runner. */
    runner: TRunners;

    /** Styles for the runner. */
    styles?: TStyles;

    /** Localization data. */
    l10n?: TL10n;

    /** Alias for the read token. */
    readTokenAlias?: string;

    /** Alias for the template token. */
    templateTokenAlias?: string;

    /** Number of times the definition was read. */
    readCount: number;

    /** Number of responses to the definition. */
    responseCount?: number;

    /** Timestamp when definition was created. */
    created: number;

    /** Timestamp when definition was last modified. */
    modified: number;

    /** Contains the fingerprint of the definition. */
    fingerprint?: string;

    /** Contains the stencil for the exportable data of the definition. */
    stencil?: string;

    /** Contains the stencil for the actionable data of the definition. */
    actionables?: string;

    /** Settings of hooks. */
    hooks?: IHookSettings;

    /** Settings of trackers. */
    trackers?: ITrackers;

    /** Specifies the tier of this definition. */
    tier?: "standard" | "premium";
}

export interface IHookSettings {
    email?: IEmailHookSettings;
    slack?: ISlackHookSettings;
    make?: IServiceHookSettings;
    zapier?: IServiceHookSettings;
    pabbly?: IServiceHookSettings;
    webhook?: IWebHookSettings;
    /**
     * @deprecated Use `make` instead.
     */
    integromat?: IServiceHookSettings;
}

interface IHook {
    enabled: boolean;
}

export interface IEmailHookSettings extends IHook {
    recipient?: string;
    includeFields?: boolean;
}

export interface ISlackHookSettings extends IHook {
    url?: string;
    includeFields?: boolean;
}

export interface IServiceHookSettings extends IHook {
    url?: string;
}

export interface IWebHookSettings extends IServiceHookSettings {
    nvp?: boolean;
}

export interface ITracker {
    enabled: boolean;
    id: string;
    useGlobal?: boolean;
    trackStart: boolean;
    trackStage: boolean;
    trackUnstage: boolean;
    trackFocus: boolean;
    trackBlur: boolean;
    trackPause: boolean;
    trackComplete: boolean;
}

export interface ITrackers {
    ga?: ITracker;
    fb?: ITracker;
    custom?: {
        enabled: boolean;
        code: string;
    };
}
