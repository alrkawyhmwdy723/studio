import { GraphQLError, GraphQLScalarType, Kind, ValueNode } from "graphql";
import { TWorkspace } from "./interface";

function parseValue(value: TWorkspace): TWorkspace {
    return value;
}

function serialize(value?: TWorkspace): TWorkspace | undefined {
    return value;
}

function parseLiteral(ast: ValueNode): TWorkspace {
    if (ast.kind === Kind.STRING) {
        return parseValue(JSON.parse(ast.value));
    }
    throw new GraphQLError(`Can only validate workspace as string, but got a: ${ast.kind}`);
}

export const workspace = new GraphQLScalarType({
    name: "workspace",
    description: "Content of a Tripetto workspace",
    parseValue,
    serialize,
    parseLiteral,
});
