/** Describes the access token provider. */
export interface IAccessTokenProvider {
    /** Create a access token. */
    readonly create: (token: string) => Promise<string | undefined>;

    /** Read a login token. */
    readonly read: (token: string) => Promise<string | undefined>;

    /** Delete a login token. */
    readonly delete: (id: string) => Promise<void>;
}
