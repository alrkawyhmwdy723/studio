import { ISnapshot } from "../entities/snapshots";

/** Describes the snapshot provider. */
export interface ISnapshotProvider {
    /** Create a snapshot. */
    readonly upsert: (
        userPublicKey: string,
        definitionPublicKey: string,
        key: string,
        data: string,
        runner: string,
        language: string,
        locale: string
    ) => Promise<string | undefined>;

    /** Read a snapshot. */
    readonly read: (
        userPublicKey: string,
        definitionPublicKey: string,
        snapshotId: string,
        includeData: boolean
    ) => Promise<ISnapshot | undefined>;

    /** Delete a snapshot. */
    readonly delete: (userPublicKey: string, definitionPublicKey: string, snapshotId: string) => Promise<void | undefined>;
}
