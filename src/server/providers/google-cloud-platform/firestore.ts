import {
    CollectionReference,
    DocumentReference,
    DocumentSnapshot,
    FieldValue,
    Firestore,
    Query,
    QueryDocumentSnapshot,
    QuerySnapshot,
    Timestamp,
} from "@google-cloud/firestore";
import { ENV, GOOGLE_CLOUD_PROJECT } from "../../settings";
import { createHash } from "../../helpers/crypto";
import { injectable } from "inversify";

@injectable()
export class FirestoreProvider {
    protected readonly firestore: Firestore;
    protected readonly userCollectionName = `users-${ENV}`;
    protected readonly accessTokenCollectionName = `access-tokens-${ENV}`;
    protected readonly tokenAliasCollectionName = `token-aliases-${ENV}`;
    protected readonly workspaceCollectionName = "workspaces";
    protected readonly definitionCollectionName = "definitions";
    protected readonly responseCollectionName = "responses";
    protected readonly announcementCollectionName = "response-announcements";
    protected readonly responseFileCollectionName = "response-files";
    protected readonly snapshotCollectionName = "snapshots";
    protected readonly metricCollectionName = `metrics-${ENV}`;
    protected readonly publicKeyFieldName = "publicKey";
    protected readonly denyCollectionName = `denylist-${ENV}`;

    constructor() {
        this.firestore = new Firestore({
            projectId: GOOGLE_CLOUD_PROJECT,
        });
    }

    private deleteQueryBatch(query: Query, batchSize: number, resolve: () => void, reject: () => void): void {
        query
            .get()
            .then((snapshot: QuerySnapshot) => {
                if (snapshot.size === 0) {
                    return 0;
                }

                const batch = this.firestore.batch();
                snapshot.docs.forEach((doc: QueryDocumentSnapshot) => {
                    batch.delete(doc.ref);
                });

                return batch.commit().then(() => snapshot.size);
            })
            .then((numDeleted: number) => {
                if (numDeleted === 0) {
                    resolve();
                    return;
                }

                // Recurse on the next process tick, to avoid exploding the stack.
                process.nextTick(() => {
                    this.deleteQueryBatch(query, batchSize, resolve, reject);
                });
            })
            .catch(reject);
    }

    /** Checks if the required user exists, before executing the `function`. Necesarry when (for example) a user is deleted,
     * but a valid token is still around.
     */
    protected async requireUserBeforeExecute<T>(userId: string, fn: (userDocument: DocumentReference) => T): Promise<T | undefined> {
        const userDocumentReference = this.firestore.doc(this.getUserPath(userId));
        return userDocumentReference.get().then((snapshot: DocumentSnapshot) => {
            if (!snapshot.exists) {
                return undefined;
            }

            return fn(userDocumentReference);
        });
    }

    protected async setPublicKey(documentReference: DocumentReference): Promise<string> {
        const publicKey = createHash(documentReference.id);
        return documentReference.update({ publicKey }).then(() => publicKey);
    }

    protected async getDocumentByPublicKey(collection: CollectionReference, publicKey: string): Promise<DocumentReference | undefined> {
        return collection
            .where(this.publicKeyFieldName, "==", publicKey)
            .get()
            .then((queryResults: QuerySnapshot) => {
                if (queryResults.empty) {
                    return undefined;
                }
                return collection.doc(queryResults.docs[0].id);
            });
    }

    protected getUserPath(userId: string): string {
        return `${this.userCollectionName}/${userId}`;
    }

    protected getLoginTokenPath(tokenId: string): string {
        return `${this.accessTokenCollectionName}/${tokenId}`;
    }

    protected getWorkspacesPath(userId: string): string {
        return `${this.getUserPath(userId)}/${this.workspaceCollectionName}`;
    }

    protected getWorkspacePath(userId: string, workspaceId: string): string {
        return `${this.getWorkspacesPath(userId)}/${workspaceId}`;
    }

    protected getDefinitionsPath(userId: string): string {
        return `${this.getUserPath(userId)}/${this.definitionCollectionName}`;
    }

    protected getDefinitionPath(userId: string, definitionId: string): string {
        return `${this.getDefinitionsPath(userId)}/${definitionId}`;
    }

    protected getResponsesPath(userId: string, definitionId: string): string {
        return `${this.getDefinitionPath(userId, definitionId)}/${this.responseCollectionName}`;
    }

    protected getResponsePath(userId: string, definitionId: string, responseId: string): string {
        return `${this.getResponsesPath(userId, definitionId)}/${responseId}`;
    }

    protected getResponseFilesPath(userId: string, definitionId: string): string {
        return `${this.getDefinitionPath(userId, definitionId)}/${this.responseFileCollectionName}`;
    }

    protected getResponseFilePath(userId: string, definitionId: string, fileId: string): string {
        return `${this.getResponseFilesPath(userId, definitionId)}/${fileId}`;
    }

    protected getSnapshotsPath(userId: string, definitionId: string): string {
        return `${this.getDefinitionPath(userId, definitionId)}/${this.snapshotCollectionName}`;
    }

    protected getSnapshotPath(userId: string, definitionId: string, snapshotId: string): string {
        return `${this.getSnapshotsPath(userId, definitionId)}/${snapshotId}`;
    }

    protected getServerTimestampValues(): { created: FieldValue; modified: FieldValue } {
        return {
            created: FieldValue.serverTimestamp(),
            modified: FieldValue.serverTimestamp(),
        };
    }

    protected getNowInMilliseconds(): number {
        return Timestamp.now().toMillis();
    }

    protected deleteCollection(collectionPath: string, batchSize: number): Promise<void> {
        const collectionReference = this.firestore.collection(collectionPath);
        const query = collectionReference.orderBy("__name__").limit(batchSize);

        return new Promise((resolve: () => void, reject: () => void) => {
            this.deleteQueryBatch(query, batchSize, resolve, reject);
        });
    }
}
