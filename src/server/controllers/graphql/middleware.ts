import { MiddlewareFn, NextFn, ResolverData } from "type-graphql";
import { GraphQLResolveInfo } from "graphql";
import { IAuthenticationToken, IUserContext } from "../../entities/tokens/interface";

export const logResolveTimeMiddleware: MiddlewareFn = async ({ info }: { info: GraphQLResolveInfo }, next: NextFn) => {
    const start = Date.now();
    await next();
    const resolveTime = Date.now() - start;
    console.log(`LOG: ${info.parentType.name}.${info.fieldName} [${resolveTime} ms]`);
};

export const logAccessMiddleware: MiddlewareFn<{ token: IAuthenticationToken }> = (
    { info, context }: { info: GraphQLResolveInfo; context: { token: IAuthenticationToken } },
    next: NextFn
) => {
    let username = "guest";
    if (context.token) {
        username = context.token.user;
    }
    console.log(`ACCESS: ${username} -> ${info.parentType.name}.${info.fieldName}`);
    return next();
};

export const logErrorMiddleware: MiddlewareFn<IUserContext> = async (action: ResolverData<IUserContext>, next: NextFn) => {
    try {
        return await next();
    } catch (err) {
        console.log(`ERROR: ${err}`);
        return Promise.reject(err);
    }
};
