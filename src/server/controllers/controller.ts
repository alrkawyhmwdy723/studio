import * as Express from "express";
import { IAccessToken, IAttachmentToken, IAuthenticationToken, IRunnerToken, ITemplateToken, IToken } from "../entities/tokens/interface";

export interface IController {
    router: Express.Router;
}

export interface ITokenRequest extends Express.Request {
    token: IToken;
}

export interface IAccessTokenRequest extends Express.Request {
    token: IAccessToken;
}

export interface IUserTokenRequest extends Express.Request {
    token: IAuthenticationToken;
}

export interface IRunnerTokenRequest extends Express.Request {
    token: IRunnerToken;
}

export interface ITemplateTokenRequest extends Express.Request {
    token: ITemplateToken;
}

export interface IAttachmentTokenRequest extends Express.Request {
    token: IAttachmentToken;
}
