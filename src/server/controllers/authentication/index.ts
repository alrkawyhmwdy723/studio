import cookieSession from "cookie-session";
import jwt from "jsonwebtoken";
import express from "express";
import ejs from "ejs";
import path from "path";
import validator from "validator";
import { inject, injectable } from "inversify";
import { IAccessTokenProvider, IEmailServiceProvider, ILogProvider, IMetricProvider, ITokenProvider, IUserProvider } from "../../providers";
import { IController, IUserTokenRequest } from "../controller";
import {
    emailServiceProviderSymbol,
    logProviderSymbol,
    loginTokenProviderSymbol as accessTokenProviderSymbol,
    metricProviderSymbol,
    tokenProviderSymbol,
    userProviderSymbol,
} from "../../symbols";
import { BaseController } from "../base";
import { EMAIL_FROM_ADDRESS, EMAIL_FROM_NAME, ENV, SESSION_COOKIE_NAME, TOKEN_COOKIE_NAME, TRIPETTO_APP_PK, URL } from "../../settings";
import * as ENDPOINTS from "../../endpoints";
import { logCriticalError, logError } from "../../helpers/error";
import { IUser } from "../../entities/users";
import { IAccessToken } from "../../entities/tokens/interface";
import { getReferer, getSessionId } from "../../helpers/request";
import { DeviceSize, MetricEventType, MetricSubjectType } from "../../providers/metric";
import { PACKAGE_VERSION } from "../../package";

@injectable()
export class AuthenticationController extends BaseController implements IController {
    private publicPaths = [ENDPOINTS.SIGNED_IN, ENDPOINTS.KEEP_ALIVE, ENDPOINTS.SIGN_OUT, ENDPOINTS.DELETE_ACCOUNT];
    private readonly viewPath = "pages/authentication";
    private readonly userProvider: IUserProvider;
    private readonly logProvider: ILogProvider;
    private readonly metricProvider: IMetricProvider;
    private readonly emailServiceProvider: IEmailServiceProvider;
    private readonly accessTokenProvider: IAccessTokenProvider;
    public router = express.Router();

    constructor(
        @inject(userProviderSymbol) userProvider: IUserProvider,
        @inject(emailServiceProviderSymbol) emailServiceProvider: IEmailServiceProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider,
        @inject(metricProviderSymbol) metricProvider: IMetricProvider,
        @inject(tokenProviderSymbol) tokenProvider: ITokenProvider,
        @inject(accessTokenProviderSymbol) accessTokenProvider: IAccessTokenProvider
    ) {
        super(tokenProvider);

        this.userProvider = userProvider;
        this.emailServiceProvider = emailServiceProvider;
        this.logProvider = logProvider;
        this.metricProvider = metricProvider;
        this.tokenProvider = tokenProvider;
        this.accessTokenProvider = accessTokenProvider;

        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        const cookieSessionHandler = cookieSession({ name: SESSION_COOKIE_NAME, secret: TRIPETTO_APP_PK, signed: false });

        this.router.post(ENDPOINTS.SIGN_IN, cookieSessionHandler, this.register());
        this.router.get(ENDPOINTS.SIGN_IN, this.verifyAccessToken());

        this.router.use(this.publicPaths, this.authenticateByCookie(false));
        this.router.use(this.publicPaths, this.verifyTokenType("auth", false));
        this.router.get(ENDPOINTS.KEEP_ALIVE, cookieSessionHandler, this.refreshToken(), this.keepAlive());
        this.router.get(ENDPOINTS.SIGNED_IN, this.signedInOtherTab());
        this.router.get(ENDPOINTS.SIGN_OUT, this.signedOut());
        this.router.post(ENDPOINTS.SIGN_OUT, this.signOut());
        this.router.get(ENDPOINTS.SIGNED_OUT, this.loggedOutOtherTab());
        this.router.get(ENDPOINTS.DELETE_ACCOUNT, this.accountDeleted());
        this.router.delete(ENDPOINTS.DELETE_ACCOUNT, this.deleteAccount());
    }

    private register(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const emailAddress = req.body && req.body.emailAddress;
            const language = (req.body && req.body.language) || "";
            const locale = (req.body && req.body.locale) || "";

            if (!emailAddress || validator.isEmail(emailAddress, {}) === false) {
                res.sendStatus(400);

                return;
            }

            this.readOrCreateUser(emailAddress, language, locale)
                .then((user: IUser) => {
                    const token = this.createLoginToken(user);
                    return this.accessTokenProvider.create(token).then(() => token);
                })
                .then((token: string) => this.sendVerificationEmail(emailAddress, token))
                .then((link: string) => {
                    this.addMetric("register", "user", req, getSessionId(req));
                    res.send({ message: ENV === "development" ? `${link}` : "" });
                })
                .catch((err: {}) => next(err));
        };
    }

    private createMagicLink(token: string): string {
        return `${URL}${ENDPOINTS.SIGN_IN}?token=${token}`;
    }

    private async sendMagicLink(emailTo: string, link: string): Promise<void> {
        return ejs
            .renderFile(path.join(__dirname, "../../../views/emails/login.ejs"), { link, recipient: emailTo, url: URL })
            .then((html: string) => {
                return this.emailServiceProvider.send({
                    to: emailTo,
                    from: {
                        name: EMAIL_FROM_NAME,
                        email: EMAIL_FROM_ADDRESS,
                    },
                    subject: "Enter Tripetto with this magic link",
                    html,
                });
            });
    }

    private async readOrCreateUser(emailAddress: string, language: string, locale: string): Promise<IUser> {
        return this.userProvider.readByEmail(emailAddress, language, locale).then((existingUser?: IUser) => {
            if (!existingUser) {
                if (!locale || (locale.indexOf("ar-") !== 0 && locale !== "ar")) {
                    this.metricProvider.add({ event: "create", subject: "user" });

                    return this.userProvider.create(emailAddress, language, locale);
                } else {
                    return new Promise((resolve: (user: IUser) => void, reject: () => void) => reject());
                }
            } else {
                return existingUser;
            }
        });
    }

    private createLoginToken(user: IUser): string {
        return this.tokenProvider.generateAccessToken(user.publicKey);
    }

    private async sendVerificationEmail(emailAddress: string, token: string): Promise<string> {
        const link = this.createMagicLink(token);
        return this.sendMagicLink(emailAddress, link)
            .then(() => link)
            .catch(logCriticalError(this.logProvider));
    }

    private verifyAccessToken(): express.RequestHandler {
        return async (req: express.Request, res: express.Response) => {
            const tokenFromQueryString = req.query.token;

            if (typeof tokenFromQueryString !== "string") {
                this.addMetric("login_token_invalid", "anonymous_user", req);
                res.render(`${this.viewPath}/link-invalid`);

                return;
            }

            jwt.verify(tokenFromQueryString, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, accessToken: IAccessToken | undefined) => {
                if (!errors) {
                    this.accessTokenProvider.read(tokenFromQueryString).then(async (id?: string) => {
                        if (id && accessToken) {
                            const [, userId] = await Promise.all([
                                this.accessTokenProvider.delete(id),
                                this.userProvider.login(accessToken.publicKey),
                            ]);

                            if (!userId) {
                                this.addMetric("login_token_invalid", "anonymous_user", req);
                                res.render(`${this.viewPath}/link-invalid`);
                            } else {
                                const authenticationToken = this.createAuthenticationToken(userId);
                                this.storeTokenInCookie(res, authenticationToken);
                                this.addMetric("login", "user", req, userId);

                                res.redirect("/");
                            }
                        } else {
                            this.addMetric("login_token_used", "anonymous_user", req);
                            res.render(`${this.viewPath}/link-used`);
                        }
                    });
                } else {
                    this.addMetric("login_token_invalid", "anonymous_user", req);
                    res.render(`${this.viewPath}/link-invalid`);
                }
            });
        };
    }

    private addMetric(
        event: MetricEventType,
        subject: MetricSubjectType,
        req: express.Request,
        subjectId?: string,
        deviceSize?: DeviceSize
    ): void {
        this.metricProvider.add({
            event,
            subject,
            ip: req.ip,
            referer: getReferer(req),
            subjectId,
            deviceSize,
        });
    }

    private keepAlive(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            const userRequest = req as IUserTokenRequest;

            if (!userRequest.token && typeof req.query.device === "string") {
                this.addMetric("update", "anonymous_user", req, getSessionId(req), req.query.device as DeviceSize);
            }

            res.json({
                isAuthenticated: userRequest.token ? true : false,
                version: PACKAGE_VERSION,
            });
        };
    }

    private signedInOtherTab(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            res.render(`${this.viewPath}/signed-in`);
        };
    }

    private clearCookie(response: express.Response): void {
        response.clearCookie(TOKEN_COOKIE_NAME);
    }

    private signOut(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            const userRequest = req as IUserTokenRequest;
            this.addMetric("logout", "user", req, userRequest.token && userRequest.token.user);

            this.clearCookie(res);
            res.sendStatus(200);
        };
    }

    private signedOut(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            res.render(`${this.viewPath}/sign-out`);
        };
    }

    private loggedOutOtherTab(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            res.render(`${this.viewPath}/signed-out`);
        };
    }

    private deleteAccount(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            const userRequest = req as IUserTokenRequest;
            if (!userRequest.token) {
                res.sendStatus(401);
            }

            const user = userRequest.token.user;
            this.metricProvider.add({
                event: "delete",
                subject: "user",
                userId: user,
                ip: req.ip,
            });
            this.logProvider.info(`User ${user} requested account deletion.`);

            // Don't wait for it.
            this.userProvider
                .delete(userRequest.token.user)
                .then(() => {
                    this.logProvider.info(`Account for user ${user} is deleted.`);
                })
                .catch(logError(this.logProvider))
                // Swallow the error.
                .catch(() => Promise.resolve());

            this.clearCookie(res);
            res.sendStatus(200);
        };
    }

    private accountDeleted(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            res.render(`${this.viewPath}/delete`);
        };
    }
}
