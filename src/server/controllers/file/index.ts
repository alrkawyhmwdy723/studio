import { tokenProviderSymbol, userProviderSymbol } from "../../symbols";
import { BaseController } from "../base";
import * as Path from "path";
import * as Express from "express";
import { IController, IUserTokenRequest } from "../controller";
import { inject, injectable } from "inversify";
import { ATTACHMENT, TERMS } from "../../endpoints";
import { ITokenProvider, IUserProvider } from "../../providers";
import { TRIPETTO_FILESTORE_URL } from "../../settings";
import { IUser } from "../../entities/users";
import request from "request";

@injectable()
export class FileController extends BaseController implements IController {
    private userProvider: IUserProvider;

    public router = Express.Router();

    constructor(@inject(tokenProviderSymbol) tokenProvider: ITokenProvider, @inject(userProviderSymbol) userProvider: IUserProvider) {
        super(tokenProvider);
        this.userProvider = userProvider;

        this.router.get(TERMS, this.terms);
        this.router.get(`${ATTACHMENT}/:attachment`, this.authenticateByCookie(true), this.attachment);
    }

    private get terms(): Express.RequestHandler {
        return (req: Express.Request, res: Express.Response) => {
            const filePath = Path.join(__dirname, "../../../static/assets/Tripetto SLA - Tripetto Studio.pdf");
            res.download(filePath);
        };
    }

    private get attachment(): Express.RequestHandler {
        return (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
            const usertoken = (req as IUserTokenRequest).token;
            this.userProvider.readById(usertoken.user).then((user?: IUser) => {
                if (!user) {
                    next();
                    return;
                }

                const token = this.tokenProvider.generateAttachmentToken(req.params.attachment, user.publicKey);

                const filestoreRequest = request.get({
                    url: TRIPETTO_FILESTORE_URL,
                    headers: {
                        "Tripetto-Attachment-Token": token,
                    },
                });
                filestoreRequest.on("response", (filestoreResponse: request.Response) => {
                    if (filestoreResponse.statusCode === 200) {
                        const contentType = filestoreResponse.headers["content-type"];
                        if (contentType) {
                            res.setHeader("content-type", contentType);
                        }
                        const contentDisposition = filestoreResponse.headers["content-disposition"];
                        if (contentDisposition) {
                            res.setHeader("content-disposition", contentDisposition);
                        }
                        filestoreRequest.pipe(res);
                    } else {
                        next();
                    }
                });
                filestoreRequest.on("error", (error: Error) => {
                    next(error);
                });
            });
        };
    }
}
