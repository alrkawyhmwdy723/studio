/** This is where we store the definition when no user is signed in. */
export const LOCAL_STORAGE_KEY = "tripetto-studio-definition";

/** This is where we store the styles when no user is signed in. */
export const LOCAL_STORAGE_KEY_STYLES = "tripetto-studio-style";

/** This is where we store the l10n datas when no user is signed in. */
export const LOCAL_STORAGE_KEY_L10N = "tripetto-studio-l10n";

/** This is where we store the current runner when no user is signed in. */
export const LOCAL_STORAGE_KEY_RUNNER = "tripetto-studio-runner";

/** Number of miliseconds between checks if session is alive. */
export const INTERVAL_KEEP_ALIVE = 90 * 1000;

/** Number of miliseconds between checks if session is alive for anonymous users. */
export const INTERVAL_KEEP_ALIVE_ANONYMOUS = 90 * 1000;

/** Path to the default definition. */
export const DEFAULT_DEFINITION = "/definitions/default.json";
