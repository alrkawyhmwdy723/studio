import { Components } from "tripetto";

export function getOrCreateFirstCollection(workspace: Components.Workspace): Components.Collection | undefined {
    return workspace.collections.count === 0 ? workspace.collections.append() : workspace.collections.firstItem;
}

export function addTile(collection: Components.Collection, definitionId: string): void {
    const tile = collection.tiles.append();
    tile.ref = {
        type: "form",
        data: definitionId,
    };
}
