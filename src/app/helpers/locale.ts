/** Dependencies */
import { L10n } from "tripetto";
import { getLocale } from "@services/locales";

export class Locale {
    /** Contains if the locale profile is loaded. */
    private static m_bLoaded = false;

    /** Retrieves if the locale profile is loaded. */
    public static get isLoaded(): boolean {
        return this.m_bLoaded;
    }

    /** Loads the locale profile. */
    public static load(done: () => void): void {
        getLocale("").then((locale) => {
            if (locale) {
                L10n.locale.load(locale);
            }

            this.m_bLoaded = true;

            done();
        });
    }
}
