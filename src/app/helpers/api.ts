import fetch from "cross-fetch";
import { DocumentNode } from "graphql";
import { ApolloClient, HttpLink, InMemoryCache, from } from "@apollo/client";
import { RetryLink } from "@apollo/client/link/retry";
import { ErrorResponse } from "@apollo/client/link/error";
import { API } from "@server/endpoints";

const client = new ApolloClient({
    link: from([
        new RetryLink({
            delay: {
                initial: 300,
                max: Infinity,
                jitter: true,
            },
            attempts: {
                max: 5,
            },
        }),
        new HttpLink({ uri: API, credentials: "same-origin", fetch }),
    ]),
    cache: new InMemoryCache(),
});

function onApiError(onError?: (authenticated: boolean) => void): (err: ErrorResponse) => undefined {
    return (error: ErrorResponse) => {
        const networkError = error.networkError;
        let authenticated = true;

        if (networkError && "statusCode" in networkError && networkError.statusCode === 401) {
            authenticated = false;
        }

        if (onError) {
            onError(authenticated);
        }
        return undefined;
    };
}

interface IQueryProperties {
    query: DocumentNode;
    variables?: {};
    onError?: (authenticated: boolean) => void;
}

export async function query<T>(props: IQueryProperties): Promise<T | undefined> {
    return client
        .query({
            query: props.query,
            variables: props.variables,
            fetchPolicy: "no-cache",
        })
        .then((value) => value.data[Object.keys(value.data)[0]])
        .catch(onApiError(props.onError));
}

export async function mutate<T>(props: IQueryProperties): Promise<T | undefined> {
    return client
        .mutate({
            mutation: props.query,
            variables: props.variables,
        })
        .then((value) => value.data && value.data[Object.keys(value.data)[0]])
        .catch(onApiError(props.onError)) as Promise<T | undefined>;
}
