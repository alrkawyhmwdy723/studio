import "./polyfills";
import { bootstrapStudio } from "./bootstrap";
import { SKIN } from "./skins/default";
import { ENV, SENTRY_DSN } from "./globals";
import * as Sentry from "@sentry/browser";

if (SENTRY_DSN) {
    Sentry.init({ dsn: SENTRY_DSN, environment: ENV });
}

bootstrapStudio(SKIN);
