import { Components, Debounce, Forms, Layers, compare, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { BuilderComponent } from "@app/components/builder";
import { RequestPremiumComponent } from "@app/components/request-premium";
import { ITracker, ITrackers } from "@server/entities/definitions/interface";
import { TRACKER_CODE_TEMPLATE } from "./template";
import { HELP_TRACKING, HELP_TRACKING_CUSTOM, HELP_TRACKING_FB, HELP_TRACKING_GA, HELP_TRACKING_GTM } from "@app/urls";
import { mutate } from "@app/helpers/api";
import * as UpdateTrackersQuery from "@app/queries/definitions/update-trackers.graphql";

export class TrackersComponent extends Components.Controller<{
    readonly studio: StudioComponent;
    readonly definitionId?: string;
    readonly definitionName?: string;
    readonly trackers: ITrackers;
    readonly premium: boolean;
}> {
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(
        studio: StudioComponent,
        trackers: ITrackers,
        premium: boolean,
        definitionId?: string,
        definitionName?: string
    ): TrackersComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new TrackersComponent(panel, studio, trackers, premium, definitionId, definitionName),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(
        layer: Layers.Layer,
        studio: StudioComponent,
        trackers: ITrackers,
        premium: boolean,
        definitionId?: string,
        definitionName?: string
    ) {
        super(
            layer,
            {
                studio,
                definitionId,
                definitionName,
                trackers,
                premium,
            },
            pgettext("studio", "Tracking"),
            "compact",
            studio.style,
            "right",
            "on-when-validated",
            pgettext("studio", "Close"),
            [new Components.ToolbarLink(studio.style.results.buttons.help, HELP_TRACKING)]
        );

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    onCards(cards: Components.Cards): void {
        const ga =
            this.ref.trackers.ga ||
            (this.ref.trackers.ga = {
                enabled: false,
                id: "",
                trackStart: true,
                trackStage: false,
                trackUnstage: false,
                trackFocus: false,
                trackBlur: false,
                trackPause: false,
                trackComplete: true,
            });

        const fb =
            this.ref.trackers.fb ||
            (this.ref.trackers.fb = {
                enabled: false,
                id: "",
                trackStart: true,
                trackStage: false,
                trackUnstage: false,
                trackFocus: false,
                trackBlur: false,
                trackPause: false,
                trackComplete: true,
            });

        const custom =
            this.ref.trackers.custom ||
            (this.ref.trackers.custom = {
                enabled: false,
                code: "",
            });

        if (!custom.code) {
            custom.code = TRACKER_CODE_TEMPLATE;
        }

        let currentTrackers = JSON.parse(JSON.stringify({ ...this.ref.trackers, ga, fb, custom }));
        const updateTrackers = new Debounce(() => {
            if (this.ref.definitionId && this.ref.premium) {
                const trackers = { ...this.ref.trackers, ga, fb, custom };

                if (!compare(trackers, currentTrackers, true)) {
                    currentTrackers = JSON.parse(JSON.stringify(trackers));

                    updateAlert.visible(true);

                    mutate({
                        query: UpdateTrackersQuery,
                        variables: {
                            id: this.ref.definitionId,
                            trackers,
                        },
                    });
                }
            }
        }, 300);

        if (!this.ref.definitionId) {
            cards.add(
                new Forms.Form({
                    controls: [
                        new Forms.Notification(
                            pgettext(
                                "studio",
                                "An account is required to use tracking. Sign in or create an account with just your email address to do so."
                            ),
                            "info"
                        ),
                    ],
                })
            );
        } else if (!this.ref.premium) {
            cards.add(
                new Forms.Form({
                    title: "🔒 " + pgettext("studio", "Upgrade required"),
                    controls: [
                        new Forms.Static(
                            "To use tracking you need to unlock this feature. Please click the button below for more information."
                        ),
                        new Forms.Button("🛒 " + pgettext("studio", "Upgrade now"), "normal")
                            .on(() => {
                                if (this.ref.studio.activeComponent instanceof BuilderComponent) {
                                    this.ref.studio.activeComponent.requestPremium();
                                } else {
                                    RequestPremiumComponent.open(
                                        this.ref.studio,
                                        this.ref.definitionId || "",
                                        this.ref.definitionName || ""
                                    );
                                }
                            })
                            .width(250),
                    ],
                })
            );
        }

        const updateAlert = cards
            .add(
                new Forms.Form({
                    controls: [
                        new Forms.Notification(
                            pgettext(
                                "studio",
                                "The tracker settings are changed. Make sure to update any embed codes you have used for this form!"
                            ),
                            "warning"
                        ),
                    ],
                })
            )
            .visible(false);

        const trackerService = (
            placeholder: string,
            description: string,
            global: string,
            tracker: ITracker,
            fnUpdate: () => void,
            events: {
                readonly start: string;
                readonly stage: string;
                readonly unstage: string;
                readonly focus: string;
                readonly blur: string;
                readonly pause: string;
                readonly complete: string;
            }
        ) => {
            const notification = new Forms.Notification(
                pgettext(
                    "studio",
                    "If you also share your form using the shareable link, make sure to supply an ID. If you only embed this form, you can omit the ID."
                ),
                "info"
            ).visible(tracker.useGlobal && !tracker.id);

            return [
                new Forms.Static(description).markdown(),
                new Forms.Text("singleline", tracker.id)
                    .placeholder(placeholder)
                    .maxLength(24)
                    .on((id) => {
                        tracker.id = id.value;

                        fnUpdate();
                    }),
                notification,
                new Forms.Checkbox(global, tracker.useGlobal || false)
                    .description(
                        pgettext(
                            "studio",
                            "Use the tracking code already available on the page hosting the Tripetto form. This prevents Tripetto placing another tracking code on the page."
                        )
                    )
                    .on((useGlobal) => {
                        tracker.useGlobal = useGlobal.isChecked;

                        notification.visible(tracker.useGlobal && !tracker.id);

                        fnUpdate();
                    }),
                new Forms.Static("**" + pgettext("studio", "Events to track:") + "**").markdown(),
                new Forms.Checkbox(pgettext("studio", "Track form starting"), tracker.trackStart)
                    .markdown()
                    .description(pgettext("studio", "Tracks when a form is started (event name: %1).", events.start))
                    .on((trackStart) => {
                        tracker.trackStart = trackStart.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(pgettext("studio", "Track form completion"), tracker.trackComplete)
                    .markdown()
                    .description(pgettext("studio", "Tracks when a form is completed (event name: %1).", events.complete))
                    .on((trackComplete) => {
                        tracker.trackComplete = trackComplete.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(pgettext("studio", "Track staged blocks"), tracker.trackStage)
                    .markdown()
                    .description(pgettext("studio", "Tracks when a block becomes available (event name: %1).", events.stage))
                    .on((trackStage) => {
                        tracker.trackStage = trackStage.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(pgettext("studio", "Track unstaged blocks"), tracker.trackUnstage)
                    .markdown()
                    .description(pgettext("studio", "Tracks when a block becomes unavailable (event name: %1).", events.unstage))
                    .on((trackUnstage) => {
                        tracker.trackUnstage = trackUnstage.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(pgettext("studio", "Track focus"), tracker.trackFocus)
                    .markdown()
                    .description(pgettext("studio", "Tracks when an input element gains focus (event name: %1).", events.focus))
                    .on((trackFocus) => {
                        tracker.trackFocus = trackFocus.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(pgettext("studio", "Track blur"), tracker.trackBlur)
                    .markdown()
                    .description(pgettext("studio", "Tracks when an input element loses focus (event name: %1).", events.blur))
                    .on((trackBlur) => {
                        tracker.trackBlur = trackBlur.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(pgettext("studio", "Track form pausing"), tracker.trackPause)
                    .markdown()
                    .description(pgettext("studio", "Tracks when a form is paused (event name: %1).", events.pause))
                    .on((trackPause) => {
                        tracker.trackPause = trackPause.isChecked;

                        fnUpdate();
                    }),
            ];
        };

        /**
         * Google Universal Analytics
         */
        const gaUpdate = () => {
            ga.enabled = gaEnabled.isChecked;

            updateTrackers.invoke();
        };
        const gaEnabled = new Forms.Checkbox(
            pgettext(
                "studio",
                "Track form activity with Google Analytics ([learn more](%1)) or Google Tag Manager ([learn more](%2))",
                HELP_TRACKING_GA,
                HELP_TRACKING_GTM
            ),
            ga.enabled
        )
            .markdown()
            .on((checkbox) => {
                gaGroup.visible(checkbox.isChecked);
                gaUpdate();
            });
        const gaGroup = new Forms.Group(
            trackerService(
                "UA-XXXXXX-X, GTM-XXXXXXX " + pgettext("studio", "or") + " G-XXXXXXXXXX",
                pgettext(
                    "studio",
                    "Configure your [Google Analytics](%1) or [Google Tag Manager](%2) setup and copy-paste your Measurement, Tracking or Container ID here:",
                    "https://analytics.google.com/",
                    "https://tagmanager.google.com/"
                ),
                pgettext("studio", "Google Analytics or Google Tag Manager is already installed on my website"),
                ga,
                gaUpdate,
                {
                    start: "tripetto_start",
                    stage: "tripetto_stage",
                    unstage: "tripetto_unstage",
                    focus: "tripetto_focus",
                    blur: "tripetto_blur",
                    pause: "tripetto_pause",
                    complete: "tripetto_complete",
                }
            )
        ).visible(ga.enabled);

        cards.add(
            new Forms.Form({
                title: "📈 " + pgettext("studio", "Google Analytics / Google Tag Manager"),
                controls: [gaEnabled, gaGroup],
            })
        ).isDisabled = !this.ref.definitionId || !this.ref.premium;

        /**
         * Facebook Pixel
         */
        const fbUpdate = () => {
            fb.enabled = fbEnabled.isChecked;

            updateTrackers.invoke();
        };
        const fbEnabled = new Forms.Checkbox(
            pgettext("studio", "Track form activity with Facebook Pixel ([learn more](%1))", HELP_TRACKING_FB),
            fb.enabled
        )
            .markdown()
            .on((checkbox) => {
                fbGroup.visible(checkbox.isChecked);
                fbUpdate();
            });
        const fbGroup = new Forms.Group(
            trackerService(
                "000000000000000",
                pgettext(
                    "studio",
                    "Configure your [Facebook Pixel](%1) setup and copy-paste your Pixel ID here:",
                    "https://www.facebook.com/business/learn/facebook-ads-pixel"
                ),
                pgettext("studio", "Facebook Pixel is already installed on my website"),
                fb,
                fbUpdate,
                {
                    start: "TripettoStart",
                    stage: "TripettoStage",
                    unstage: "TripettoUnstage",
                    focus: "TripettoFocus",
                    blur: "TripettoBlur",
                    pause: "TripettoPause",
                    complete: "TripettoComplete",
                }
            )
        ).visible(fb.enabled);

        cards.add(
            new Forms.Form({
                title: "📈 " + pgettext("studio", "Facebook Pixel"),
                controls: [fbEnabled, fbGroup],
            })
        ).isDisabled = !this.ref.premium;

        /**
         * Custom code
         */
        const customCodeUpdate = () => {
            custom.enabled = customCodeEnabled.isChecked;
            custom.code = customCode.value;

            updateTrackers.invoke();
        };
        const customCodeEnabled = new Forms.Checkbox(
            pgettext("studio", "Track form activity with custom tracking code ([learn more](%1))", HELP_TRACKING_CUSTOM),
            custom.enabled
        )
            .markdown()
            .on((checkbox) => {
                customCodeGroup.visible(checkbox.isChecked);
                customCodeUpdate();
            });
        const customCode = new Forms.Text("multiline", custom.code)
            .sanitize(false)
            .trim(false)
            .on(() => customCodeUpdate());
        const customCodeGroup = new Forms.Group([
            new Forms.Static(
                pgettext(
                    "studio",
                    "Implement your custom JavaScript code for event tracking and connect Tripetto to other tracking services."
                )
            ),
            customCode,
            new Forms.Static(
                "⚠️ **" +
                    pgettext(
                        "studio",
                        "Custom tracking code is only allowed for embedded forms and does not work when using the shareable link!"
                    ) +
                    "**"
            ).markdown(),
        ]).visible(custom.enabled);

        cards.add(
            new Forms.Form({
                title: "👩‍💻 " + pgettext("studio", "Custom tracking code"),
                controls: [customCodeEnabled, customCodeGroup],
            })
        ).isDisabled = !this.ref.premium;
    }
}
