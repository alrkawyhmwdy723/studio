import { Components, DOM, linearicon, pgettext } from "tripetto";
import { IHeaderStyle } from "./style";
import { DeviceSize } from "@server/providers/metric";

export class Title extends Components.ToolbarStatic<DeviceSize> {
    private icon!: DOM.Element;
    private prefix!: DOM.Element;

    constructor(style: IHeaderStyle, title: string, fnTapped: () => void) {
        super(style.title, title, undefined, fnTapped);
    }

    private getIcon(mode: "loading" | "workspace" | "build"): number {
        switch (mode) {
            case "workspace":
                return 0xe977;
            case "build":
                return 0xe62d;
            default:
                return 0xe8cf;
        }
    }

    private getPrefix(mode: "loading" | "workspace" | "build"): string {
        switch (mode) {
            case "workspace":
                return pgettext("studio", "Workspace");
            case "build":
                return pgettext("studio", "Build");
            default:
                return pgettext("studio", "Loading");
        }
    }

    protected onViewChange(): boolean {
        if (this.element) {
            this.element.selectorSafe("compact", this.view === "xs");

            return true;
        }

        return false;
    }

    onDraw(toolbar: Components.Toolbar<DeviceSize>, element: DOM.Element): void {
        this.icon = element.create("i", (icon: DOM.Element) => linearicon(this.getIcon("loading"), icon, true));
        this.prefix = element.create("span", (prefix: DOM.Element) => (prefix.text = this.getPrefix("loading")));

        super.onDraw(toolbar, element, element.create("span"));
    }

    setMode(mode: "loading" | "workspace" | "build"): void {
        if (this.element) {
            linearicon(this.getIcon(mode), this.icon, true);

            this.prefix.text = this.getPrefix(mode);
            this.element.selectorSafe("workspace", mode === "workspace");
            this.element.selectorSafe("build", mode === "build");
            this.element.selectorSafe("compact", this.view === "xs");
        }
    }
}
