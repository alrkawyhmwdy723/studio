import * as Superagent from "superagent";
import { Components, Forms, L10n, Layers, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { BLOCKED, SIGN_IN } from "@server/endpoints";
import { ENV } from "@app/globals";
import { TERMS } from "@app/urls";
import { IStudioStyle } from "@app/components/studio/style";

export class SignInComponent extends Components.Controller<{
    emailAddress: string;
}> {
    private loginCard!: Components.Card;
    private loginAddress!: Forms.Email;
    private loginButton!: Forms.Button;
    private loginConfirmation!: Components.Card;
    private loginNotification!: Forms.Notification;
    whenClosed?: () => void;

    static open(studio: StudioComponent): SignInComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new SignInComponent(panel, studio),
            Layers.Layer.configuration.width(400).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent) {
        super(
            layer,
            {
                emailAddress: "",
            },
            pgettext("studio", "Sign in to your account"),
            "compact",
            studio.style,
            "right",
            "always-on",
            pgettext("studio", "Close")
        );

        layer.onClose = () => this.whenClosed && this.whenClosed();
    }

    private signIn(): void {
        const emailAddress = this.ref.emailAddress;

        Superagent.post(SIGN_IN)
            .send({ emailAddress, language: L10n.current, locale: L10n.locale.identifier })
            .retry(2)
            .then((response: Superagent.Response) => {
                if (ENV === "development") {
                    console.log(response.body && response.body.message);
                }

                if (response.ok) {
                    this.loginNotification.label(
                        pgettext("studio", "We've sent the magic link to %1! Please check your inbox.", emailAddress)
                    );

                    this.loginCard.hide();
                    this.loginConfirmation.show();
                }
            })
            .catch(() => {
                window.location.assign(BLOCKED);
            });

        this.loginAddress.disable();
        this.loginButton.disable();
    }

    onCards(cards: Components.Cards): void {
        this.loginNotification = new Forms.Notification("", "success");
        this.loginConfirmation = cards
            .add(
                new Forms.Form({
                    controls: [this.loginNotification],
                })
            )
            .hide();

        this.loginCard = cards.add(
            new Forms.Form({
                controls: [
                    new Forms.Static(
                        pgettext(
                            "studio",
                            "Enter your email address below and we'll send you a magic link right away. Jump to your inbox, click the link and off you go. No passwords here."
                        )
                    ),
                    new Forms.Spacer("small"),
                    (this.loginAddress = new Forms.Email(Forms.Email.bind(this.ref, "emailAddress", ""))
                        .hook("OnValidate", "synchronous", (ev: Forms.IControlValidateEvent<Forms.Email>) => {
                            this.loginButton.disabled(!ev.control.isPassed);
                        })
                        .enter((email: Forms.Email) => {
                            if (email.isPassed) {
                                this.signIn();

                                return true;
                            }

                            return false;
                        })
                        .escape(() => {
                            this.close();

                            return true;
                        })
                        .placeholder(pgettext("studio", "Enter your email address here"))
                        .require()
                        .autoValidate()
                        .autoFocus()
                        .autoSelect()),
                    new Forms.Spacer("small"),
                    (this.loginButton = new Forms.Button(pgettext("studio", "Receive magic link!"))
                        .disable()
                        .width("full")
                        .on(() => this.signIn())),
                ],
            })
        );

        cards.add(
            new Forms.Form({
                controls: [
                    new Forms.HTML(
                        `<b>${pgettext("studio", "Free for now")}<\u002fb><br \u002f>` +
                            pgettext(
                                "studio",
                                "The Tripetto Studio is free for now and for as long as possible. And we promise we won't forget early adopters."
                            ) +
                            `<br \u002f><br \u002f>` +
                            `<b>${pgettext("studio", "Terms of use")}<\u002fb><br \u002f>` +
                            pgettext(
                                "studio",
                                "By using the Tripetto Studio, you acknowledge that you have read and understand the applicable terms and conditions found at %1 and you agree to be bound by these.",
                                `<a href="${TERMS}" target="_blank">tripetto.app\u002fterms<\u002fa>`
                            ),
                        false,
                        {
                            appearance: (this.style as IStudioStyle).signIn.terms,
                        }
                    ),
                ],
                style: {
                    appearance: {
                        border: "none",
                    },
                },
            })
        );
    }
}
