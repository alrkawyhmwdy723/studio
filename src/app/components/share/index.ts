import { Components, Debounce, Forms, IDefinition, Layers, Str, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import { BuilderComponent } from "../builder";
import { ENV, FILESTORE_URL, URL } from "@app/globals";
import { RUN } from "@server/endpoints";
import { TRunners } from "@server/entities/definitions/runners";
import { ITrackers } from "@server/entities/definitions/interface";
import { DOCUMENTATION, HELP_SHARE, HELP_SHARE_EMBED, HELP_SHARE_LINK } from "@app/urls";
import { IShareSettings } from "@server/entities/users";
import { mutate } from "@app/helpers/api";
import { addMetric } from "@app/helpers/metric";
import { MetricEventType } from "@server/providers/metric";
import { ShareEmbedTypes, ShareModes } from "@server/entities/users/interface";
import { TL10n, TStyles } from "tripetto-runner-foundation";
import { getTrackerCode } from "@server/helpers/trackers";
import * as UpdateShareSettingsQuery from "@app/queries/users/update-share-settings.graphql";

export class ShareComponent extends Components.Controller<{
    readonly runner: TRunners;
    readonly definition?: IDefinition | BuilderComponent;
    readonly styles?: TStyles;
    readonly l10n?: TL10n;
    readonly token?: string;
    readonly alias?: string;
    readonly trackers?: ITrackers;
    readonly settings?: IShareSettings;
}> {
    private readonly studio: StudioComponent;
    private codePen?: HTMLInputElement;
    private definitionId?: string;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(
        studio: StudioComponent,
        runner?: TRunners,
        definition?: IDefinition | BuilderComponent,
        styles?: TStyles,
        l10n?: TL10n,
        token?: string,
        alias?: string,
        trackers?: ITrackers
    ): ShareComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new ShareComponent(panel, studio, runner, definition, styles, l10n, token, alias, trackers),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(
        layer: Layers.Layer,
        studio: StudioComponent,
        runner?: TRunners,
        definition?: IDefinition | BuilderComponent,
        styles?: TStyles,
        l10n?: TL10n,
        token?: string,
        alias?: string,
        trackers?: ITrackers
    ) {
        super(
            layer,
            {
                runner: runner || "autoscroll",
                definition,
                styles,
                l10n,
                token,
                alias,
                trackers,
            },
            pgettext("studio", "Share"),
            "compact",
            studio.style,
            "right",
            "always-on",
            pgettext("studio", "Close"),
            [new Components.ToolbarLink(studio.style.results.buttons.help, HELP_SHARE)]
        );

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });

        this.studio = studio;
        this.definitionId = this.ref.definition instanceof BuilderComponent ? this.ref.definition.id : undefined;
    }

    private calculateLines(src: string): number {
        return src.split("\n").length || 1;
    }

    private update(dest: Forms.Text, value: string): void {
        dest.fixedLines(this.calculateLines((dest.value = value)));
        dest.visible(value ? true : false);
    }

    private addSharingMetric(event: MetricEventType): void {
        addMetric(event, "definition", this.definitionId);
    }

    private copyToClipboard(source: Forms.Text, eventType: MetricEventType): Forms.Button {
        const label = pgettext("studio", "Copy to clipboard");

        return new Forms.Button(label).width(200).onClick((button: Forms.Button) => {
            button.disable();
            button.type("accept");
            button.label(pgettext("studio", "✔ Copied!"));

            source.copyToClipboard();

            this.addSharingMetric(eventType);

            setTimeout(() => {
                button.enable();
                button.type("normal");
                button.label(label);
            }, 1000);
        });
    }

    private tryOnCodePen(source: Forms.Text): void {
        if (!this.codePen) {
            const form = document.createElement("form");

            this.codePen = document.createElement("input");
            this.codePen.type = "hidden";
            this.codePen.name = "data";

            form.action = "https://codepen.io/pen/define";
            form.target = "_blank";
            form.method = "POST";
            form.style.visibility = "hidden";

            form.appendChild(this.codePen);

            document.body.appendChild(form);

            this.layer.hook("OnClose", "framed", () => form.remove());
        }

        this.codePen.value = JSON.stringify({
            title: "Tripetto Runner",
            html: source.value,
            editors: "100",
            css: "",
            js: "",
        });

        (this.codePen.parentElement as HTMLFormElement).submit();

        this.addSharingMetric("share_codepen");
    }

    private code(
        type: ShareEmbedTypes,
        options: {
            hostDefinition: boolean;
            hostResults: boolean;
            supportPauseAndResume: boolean;
            persistent: boolean;
        },
        cdn: string | undefined
    ): string {
        const definition = JSON.stringify(
            this.ref.definition instanceof BuilderComponent ? this.ref.definition.ref.definition : this.ref.definition || {}
        );
        const styles = JSON.stringify(
            this.ref.styles ? this.ref.styles : this.ref.definition instanceof BuilderComponent ? this.ref.definition.styles : {}
        );
        const l10n = JSON.stringify(
            this.ref.l10n ? this.ref.l10n : this.ref.definition instanceof BuilderComponent ? this.ref.definition.l10n : {}
        );

        if (type === "form-kit") {
            return definition;
        }

        const submitCode =
            `${type === "snippet" || type === "snippet-page" || type === "page" ? `function(instance)` : `(instance) =>`} {` +
            `\n        // TODO: Handle the results` +
            `\n        // For example retrieve the results as a CSV-file:` +
            `\n        ${type === "snippet" || type === "snippet-page" || type === "page" ? "var" : "const"} csv = ${
                type === "snippet" || type === "snippet-page" || type === "page" ? "TripettoRunner." : ""
            }Export.CSV(instance);` +
            `\n        // Or retrieve the individual fields:` +
            `\n        ${type === "snippet" || type === "snippet-page" || type === "page" ? "var" : "const"} fields = ${
                type === "snippet" || type === "snippet-page" || type === "page" ? "TripettoRunner." : ""
            }Export.fields(instance);` +
            `\n    }`;

        const pauseCode =
            `${type === "snippet" || type === "snippet-page" || type === "page" ? `function(snapshot)` : `(snapshot) =>`} {` +
            `\n        // TODO: Store the snapshot data somewhere` +
            `\n    }`;
        /*
        const attachmentCode =
            `{` +
            `\n        put: ${
                type === "snippet" || type === "snippet-page" || type === "page"
                    ? `function(file, onProgress)`
                    : `(file${(type === "ts" && ": File") || ""}, onProgress${(type === "ts" && ": (progress: number) => void") || ""}) =>`
            } {` +
            `\n            \u002f\u002f TODO: Handle the file and return an identifier as a Promise.` +
            `\n            \u002f\u002f Notify the progress` +
            `\n            onProgress(100);` +
            `\n            \u002f\u002f Return an identifier to store in the reponse.` +
            `\n            return "1";` +
            `\n        }` +
            `\n        delete: ${
                type === "snippet" || type === "snippet-page" || type === "page"
                    ? `function(file)`
                    : `(file${(type === "ts" && ": string") || ""}) =>`
            } {` +
            `\n            \u002f\u002f TODO: Delete the file.` +
            `\n        }` +
            `\n    }`;
            */

        let code = "";

        cdn = Str.replace(cdn || "", "/", "\u002f");

        if (type === "page") {
            code += `<!DOCTYPE html>\n`;
            code += `<html>\n`;
            code += `<head>\n`;
            code += `<meta charset="UTF-8" \u002f>\n`;
            code += `<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" \u002f>\n`;
            code += `<\u002fhead>\n`;
            code += `<body>\n`;
        }

        if (type === "snippet" || type === "snippet-page" || type === "page") {
            if (type === "snippet") {
                code += `<div id="tripetto"></div>\n`;
            }

            code +=
                `<script src="${cdn}tripetto-runner-foundation${cdn === "" ? ".js" : ""}"><\u002fscript>` +
                `\n<script src="${cdn}tripetto-runner-${this.ref.runner}${cdn === "" ? ".js" : ""}"><\u002fscript>`;

            if (!options.hostDefinition || !options.hostResults) {
                const suffix = ENV === "staging" ? "@staging" : "";

                code += `\n<script src="${cdn}tripetto-services${cdn === "" ? ".js" : suffix}"><\u002fscript>`;
            }

            code += "\n<script>";
        } else {
            code += `import { run } from "tripetto-runner-${this.ref.runner}";\n`;

            if (type === "es6" && options.hostResults) {
                code += `import { Export } from "tripetto-runner-foundation";\n`;
            }

            if (!options.hostDefinition || !options.hostResults) {
                code += `import Services from "tripetto-services";\n`;
            }
        }

        if (!options.hostDefinition || !options.hostResults) {
            if (type === "snippet" || type === "snippet-page" || type === "page") {
                code += `\nvar tripetto = TripettoServices.init({ token: "${this.ref.token || ""}"${
                    ENV !== "production" ? `, url: "${URL}", filestoreUrl: "${FILESTORE_URL}"` : ""
                } });\n`;
            } else {
                code += "\nconst { ";

                if (!options.hostDefinition) {
                    code += "definition, styles, l10n, locale, translations" + (!options.hostResults ? ", " : "");
                }

                if (!options.hostResults) {
                    code += "snapshot, attachments, onSubmit, onPause";
                }

                code += ` } = Services.init({ token: "${this.ref.token || ""}"`;

                if (ENV !== "production") {
                    code += `, url: "${URL}", filestoreUrl: "${FILESTORE_URL}"`;
                }

                code += " });\n";
            }
        }

        if (type === "snippet" || type === "snippet-page" || type === "page") {
            code += `\nTripetto${Str.capitalize(this.ref.runner)}.run({`;

            if (type === "snippet") {
                code += `\n    element: document.getElementById("tripetto"),`;
            } else {
                code += `\n    element: document.body,` + (type !== "page" ? " /* Or supply your own element here */" : "");
            }

            code += `\n    definition: ${options.hostDefinition ? definition : "tripetto.definition"}`;

            if (!options.hostDefinition || styles !== "{}") {
                code += `,\n    styles: ${options.hostDefinition ? styles : "tripetto.styles"}`;
            }

            if (!options.hostDefinition || l10n !== "{}") {
                code += `,\n    l10n: ${options.hostDefinition ? l10n : "tripetto.l10n"}`;

                if (!options.hostDefinition) {
                    code += `,\n    locale: tripetto.locale`;
                    code += `,\n    translations: tripetto.translations`;
                }
            }

            if (!options.hostResults) {
                code += ",\n    attachments: tripetto.attachments";
            }

            code += `,\n    onSubmit: ${options.hostResults ? submitCode : "tripetto.onSubmit"}`;

            if (options.supportPauseAndResume) {
                code += `,\n    snapshot: ${options.hostResults ? "undefined /* Feed snapshot data here */" : "tripetto.snapshot"}`;
                code += `,\n    onPause: ${options.hostResults ? pauseCode : "tripetto.onPause"}`;
            }
        } else {
            code += `\nrun({`;
            code += `\n    element: document.body, /* Or supply your own element here */`;
            code += `\n    definition${options.hostDefinition ? `: ${definition}` : ""}`;

            if (!options.hostDefinition || styles !== "{}") {
                code += `,\n    styles${options.hostDefinition ? `: ${styles}` : ""}`;
            }

            if (!options.hostDefinition || l10n !== "{}") {
                code += `,\n    l10n${options.hostDefinition ? `: ${l10n}` : ""}`;

                if (!options.hostDefinition) {
                    code += `,\n    locale`;
                    code += `,\n    translations`;
                }
            }

            if (!options.hostResults) {
                code += ",\n    attachments";
            }

            code += `,\n    onSubmit${options.hostResults ? `: ${submitCode}` : ""}`;

            if (options.supportPauseAndResume) {
                code += `,\n    snapshot${options.hostResults ? ": undefined /* Feed snapshot data here */" : ""}`;
                code += `,\n    onPause${options.hostResults ? ": " + pauseCode : ""}`;
            }
        }

        if (options.persistent) {
            code += ",\n    persistent: true";
        }

        if (this.ref.trackers) {
            const trackerCode = getTrackerCode(this.ref.trackers, true, false);

            if (trackerCode) {
                code += `,\n    onAction: (function(trackers){return function(a,b,c){trackers.forEach(function(tracker){tracker(a,b,c)})}})(${trackerCode})`;
            }
        }

        code += `\n});`;

        if (type === "snippet" || type === "snippet-page" || type === "page") {
            code += "\n<\u002fscript>";
        }

        if (type === "page") {
            code += "\n<\u002fbody>";
            code += "\n<\u002fhtml>";
        }

        return code;
    }

    private bash(
        type: ShareEmbedTypes,
        options: {
            hostDefinition: boolean;
            hostResults: boolean;
        }
    ): string {
        if (type !== "es6") {
            return "";
        }

        return `npm i tripetto-runner-foundation tripetto-runner-${this.ref.runner}${
            !options.hostDefinition || !options.hostResults ? ` tripetto-services` : ""
        } --save`;
    }

    onCards(cards: Components.Cards): void {
        const runnerURL = URL + RUN + "/" + (this.ref.alias || "");
        const urlText = new Forms.Text("singleline", runnerURL).readonly();
        const codeText = new Forms.Text("multiline", "")
            .fixedLines(10)
            .sanitize(false)
            .trim(false)
            .readonly()
            .label(pgettext("studio", "Paste the following code to your website or application."));
        const bashText = new Forms.Text("multiline", "")
            .fixedLines(2)
            .sanitize(false)
            .trim(false)
            .readonly()
            .label(pgettext("studio", "Make sure to install the required npm packages:"));
        const embedType = new Forms.Dropdown<ShareEmbedTypes>(
            [
                {
                    optGroup: "HTML",
                },
                {
                    value: "snippet",
                    label: pgettext("studio", "Snippet (inline with other content)"),
                },
                {
                    value: "snippet-page",
                    label: pgettext("studio", "Snippet (whole page)"),
                },
                {
                    value: "page",
                    label: pgettext("studio", "Page"),
                },
                {
                    optGroup: "Embed in application",
                },
                {
                    value: "es6",
                    label: pgettext("studio", "JavaScript (ES6) / TypeScript"),
                },
                {
                    optGroup: "Go commando",
                },
                {
                    value: "form-kit",
                    label: pgettext("studio", "Use the Tripetto Form Kit"),
                },
            ],
            this.ref.settings?.embed?.type || "snippet"
        ).on(() => fnUpdate());

        const updateSettings = new Debounce(() => {
            if (this.definitionId) {
                const settings: IShareSettings = {
                    mode: mode.value || "url",
                    embed: {
                        type: embedType.value || "snippet",
                        hostDefinition: optionDefinition.isChecked,
                        hostResponse: optionResults.isChecked,
                        supportPauseAndResume: optionPausing.isChecked,
                        persistent: optionPersistent.isChecked,
                        cdn: cdnURL.value || "",
                    },
                };

                mutate({ query: UpdateShareSettingsQuery, variables: { settings } });
            }
        });

        const fnUpdate = () => {
            const typeOfEmbed = embedType.value || "snippet";

            this.update(
                codeText,
                this.code(
                    typeOfEmbed,
                    {
                        hostDefinition: optionDefinition.isChecked,
                        hostResults: optionResults.isChecked,
                        supportPauseAndResume: optionPausing.isChecked,
                        persistent: optionPersistent.isChecked,
                    },
                    cdnURL.value
                )
            );

            this.update(
                bashText,
                this.bash(typeOfEmbed, {
                    hostDefinition: optionDefinition.isChecked,
                    hostResults: optionResults.isChecked,
                })
            );

            options.visible(mode.value === "embed" && typeOfEmbed !== "form-kit");
            cdn.visible(mode.value === "embed" && typeOfEmbed !== "es6" && typeOfEmbed !== "form-kit");
            formKit.visible(typeOfEmbed === "form-kit");
            formKitButton.visible(typeOfEmbed === "form-kit");
            codePen.visible(typeOfEmbed === "snippet" || typeOfEmbed === "snippet-page" || typeOfEmbed === "page");

            updateSettings.invoke();
        };

        if (this.ref.definition instanceof BuilderComponent) {
            const builder = this.ref.definition.ref;

            builder.hook("OnLoad", "synchronous", () => fnUpdate(), this);
            builder.hook("OnChange", "synchronous", () => fnUpdate(), this);

            this.layer.hook("OnClose", "synchronous", () => builder.unhookContext(this));
        }

        const mode = new Forms.Radiobutton<ShareModes>(
            [
                {
                    label: pgettext("studio", "🧭 Share a link ([learn more](%1))", HELP_SHARE_LINK),
                    description: pgettext(
                        "studio",
                        "Your form will be available through a dedicated link. Both the form and collected data are stored under your account at Tripetto in Western Europe. This service is free, but a fair-use policy applies."
                    ),
                    markdown: true,
                    value: "url",
                },
                {
                    label: pgettext("studio", "👨‍💻 Embed in a website or application ([learn more](%1))", HELP_SHARE_EMBED),
                    description: pgettext(
                        "studio",
                        "You decide where your form is published, and where the form and incoming data are stored. We supply you with the required code for publication. This service is free."
                    ),
                    markdown: true,
                    value: "embed",
                },
            ],
            this.ref.settings?.mode || "url"
        ).on(() => {
            url.visible(mode.value === "url");
            type.visible(mode.value === "embed");
            options.visible(mode.value === "embed" && embedType.value !== "form-kit");
            cdn.visible(mode.value === "embed" && embedType.value !== "form-kit");
            code.visible(mode.value === "embed");
        });

        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Method"),
                controls: [new Forms.Static(pgettext("studio", "How would you like to share your form?")), mode],
            })
        );

        const url = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Shareable link"),
                controls: this.ref.token
                    ? [
                          new Forms.Button(pgettext("studio", "Open in browser"), "accept")
                              .width(200)
                              .url(runnerURL)
                              .on(() => {
                                  this.addSharingMetric("share_open_link");
                              }),
                          new Forms.Static(pgettext("studio", "Or just share the link below with anyone.")),
                          urlText,
                          this.copyToClipboard(urlText, "share_copy_link"),
                      ]
                    : [
                          new Forms.Static(
                              pgettext(
                                  "studio",
                                  "An account is required to host shared forms and collected data at Tripetto. Sign in or create an account with just your email address to do so."
                              )
                          ),
                          new Forms.Spacer("small"),
                          new Forms.Button(pgettext("studio", "Sign in or register")).on(() => this.studio.signIn()),
                      ],
            })
        );

        const optionDefinition = new Forms.Checkbox(
            pgettext("studio", "Self-host the form definition"),
            this.ref.settings?.embed?.hostDefinition || false
        )
            .description(
                pgettext(
                    "studio",
                    "The form definition holds your form's structure. By self-hosting it you can source the form without the Tripetto platform, since its definition's included in the embed code."
                )
            )
            .on(() => fnUpdate());
        const optionResults = new Forms.Checkbox(
            pgettext("studio", "Self-host collected data"),
            this.ref.settings?.embed?.hostResponse || false
        )
            .description(
                pgettext(
                    "studio",
                    "By self-hosting collected data you remain its sole keeper. Collected data isn't even sent to Tripetto at all. Instructions for data handling are included in the embed code."
                )
            )
            .on(() => fnUpdate());
        const optionPausing = new Forms.Checkbox(
            pgettext("studio", "Allow pausing and resuming"),
            this.ref.settings?.embed?.supportPauseAndResume || false
        )
            .description(
                pgettext(
                    "studio",
                    "Allows users to pause the form and continue with it later on. If you choose to host the collected data at Tripetto, we will ask the user for its email address and send a resume link."
                )
            )
            .on(() => fnUpdate());
        const optionPersistent = new Forms.Checkbox(
            pgettext("studio", "Save and restore uncompleted forms"),
            this.ref.settings?.embed?.persistent || false
        )
            .description(
                pgettext(
                    "studio",
                    "Saves uncompleted forms in the local storage of the browser. Next time the user visits the form it is restored so the user can continue."
                )
            )
            .on(() => fnUpdate());

        const type = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Embed type"),
                controls: [new Forms.Static(pgettext("studio", "How do you want to embed your form?")), embedType],
            })
        );

        const options = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Options"),
                controls: [
                    new Forms.Static(
                        pgettext(
                            "studio",
                            "The default hosting destination for your embedded form and collected data is your Tripetto account. The form definition is sourced there for publication at your designated location. You may bypass the Tripetto platform by self-hosting ([learn more](%1)).",
                            "https://tripetto.com/help/articles/how-to-take-control-over-your-data-from-the-studio/"
                        )
                    ).markdown(),
                    optionDefinition,
                    optionResults,
                    optionPausing,
                    optionPersistent,
                    ...(this.ref.token
                        ? []
                        : [
                              new Forms.Notification(
                                  pgettext(
                                      "studio",
                                      "An account is required to host embedded forms and collected data at Tripetto. Without it you will have to self-host the form and data."
                                  ),
                                  "info"
                              ),
                          ]),
                ],
            })
        );

        const cdnCustom = new Forms.Static(
            pgettext(
                "studio",
                "If you want to use a custom domain or CDN, you need to publish the required JavaScript files for Tripetto to that domain or CDN. To do so, download the files below and publish them to the desired domain or CDN. Update your embed code so the `src`-attributes of the `script` tags point to the right domain and location of the JavaScript files.\n\n💎 [tripetto-runner-foundation.js](https://unpkg.com/tripetto-runner-foundation)\n\n💎 [tripetto-runner-%1.js](https://unpkg.com/tripetto-runner-%1)\n\n💎 [tripetto-services.js](https://unpkg.com/tripetto-services) (not used when self-hosting the form definition and collected data)",
                this.ref.runner
            )
        )
            .markdown()
            .visible(false);
        const cdnURL = new Forms.Radiobutton(
            [
                {
                    label: "jsDelivr ([website](https://www.jsdelivr.com/))",
                    markdown: true,
                    value: "https://cdn.jsdelivr.net/npm/",
                },
                {
                    label: "unpkg ([website](https://unpkg.com/))",
                    markdown: true,
                    value: "https://unpkg.com/",
                },
                {
                    label: pgettext("studio", "Custom domain or CDN"),
                    markdown: true,
                    value: "",
                },
            ],
            this.ref.settings?.embed?.cdn || "https://cdn.jsdelivr.net/npm/"
        )
            .label(
                pgettext(
                    "studio",
                    "The required packages for embedding Tripetto are loaded from a [content delivery network](%1). Please select the preferred network to use for your embed script:",
                    "https://en.wikipedia.org/wiki/Content_delivery_network"
                )
            )
            .markdown()
            .on(() => {
                cdnCustom.visible(cdnURL.value === "");

                fnUpdate();
            });
        const cdn = cards.add(
            new Forms.Form({
                title: pgettext("studio", "CDN"),
                controls: [cdnURL, cdnCustom],
            })
        );

        const formKit = new Forms.Static(
            pgettext(
                "studio",
                "The Tripetto Studio in front of you is built with the Tripetto Form Kit. The exact same kit is available to developers as a set of Javascript libraries through npm and code on GitLab, to let them integrate Tripetto tech straight into their own application."
            )
        ).hide();
        const formKitButton = new Forms.Button(pgettext("studio", "Learn more"), "accept").width(200).url(DOCUMENTATION).hide();
        const codePen = new Forms.Button(pgettext("studio", "Run in CodePen"), "accept")
            .width(200)
            .onClick(() => this.tryOnCodePen(codeText));
        const code = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Embed code"),
                controls: [formKit, formKitButton, this.copyToClipboard(codeText, "share_copy_code"), codeText, bashText, codePen],
            })
        );

        if (!this.ref.token) {
            optionDefinition.check().disable();
            optionResults.check().disable();
        }
    }
}
